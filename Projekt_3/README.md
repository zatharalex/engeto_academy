# Project 3 - Web scrapping Engeto Academy

This project was made as final project for Engeto Academy. Author is Tomas Zak.

## Introduction

This script was made for extracting data from web page: [Volby do poslanecké sněmovny 2017](https://volby.cz/pls/ps2017nss/ps3?xjazyk=CZ)
After data are downloaded, they are filtered and stored in csv file. The name of csv file is defined by user.
Script extract from the page, informations about each city in this order:

| Code of the city | Name of the city | Voters in list | Issued envelopes | Valid votes | Candidate parties |

## Before you run the program
Before you run program please make sure that all libraries and requirements are satisfied to run this command:
```
pip install -r requirements.txt
```

## How to run program

This python program needs to run with argument, without arguments it won't run.
This program needs two arguments:

- [ ] 1.) First argument is url of selected region for example Benešov: 'https://volby.cz/pls/ps2017nss/ps32?xjazyk=CZ&xkraj=2&xnumnuts=2101' (including qoutes)
- [ ] 2.) Second argument is the name of the csv file, where filtered data from selected region are stored, for example: "Benešov.csv" (including qoutes)


Example how to run program:
```
python3 projekt3.py 'https://volby.cz/pls/ps2017nss/ps32?xjazyk=CZ&xkraj=2&xnumnuts=2101' "Benešov.csv"
```

If you entered data correctly, csv file is created and you should see similar output:
```
--------------------------------------------------------------------------------
                Welcome - Scrapping from Parlamentni volby 2017                 
--------------------------------------------------------------------------------
Downloading data from url: https://volby.cz/pls/ps2017nss/ps32?xjazyk=CZ&xkraj=2&xnumnuts=2101
Downloaded data will be written to file: Benešov.csv
--------------------------------------------------------------------------------
Downloading... Please wait.
--------------------------------------------------------------------------------
Writting downloaded data to file: Benešov.csv
--------------------------------------------------------------------------------
Process successfully finished. Done.
Thank you for choosing my program.
--------------------------------------------------------------------------------

```

If you entered data incorrectly or in different order you get error and noticed what could be wrong:

```
--------------------------------------------------------------------------------
                Welcome - Scrapping from Parlamentni volby 2017                 
--------------------------------------------------------------------------------
Ops, something went wrong. Please check, that first argument is url region and second argument name of the file
or you enter correct name of the file or you put url from Parlamentni volby 2017.

```

## Support
If you need support you can contact me at: tomaszak@atlas.cz

## Authors and acknowledgment
Thank you Engeto for perfect course and I appreciete it, that you told as so much useful informations and lot of them beyond limits of this course.

## License
This is free to use for personal usage. Author is not responsible for any harms on your device, OS or if you destroy this world with the script. :)
