"""
This program was made as final project 3 in Engeto Academy - Web scrapping
Author: Tomas Zak
"""


# ---------------------------------------------------------------------------------------------------------------------
# Import libraries:
# ---------------------------------------------------------------------------------------------------------------------
import requests
import bs4
import csv
import sys
import pandas as pd


# ---------------------------------------------------------------------------------------------------------------------
# Global variables:
# ---------------------------------------------------------------------------------------------------------------------

# This is final list, which we are writting in user defined csv file
city_list_complete = []

# Header for csv file
header = ["Kód obce/města", "Název obce/města", "Voliči v seznamu", "Vydané obálky", "Platné hlasy"]

separator = "-" * 80


# ---------------------------------------------------------------------------------------------------------------------
# Local functions:
# ---------------------------------------------------------------------------------------------------------------------


def find_cities(polivka):
    """ This function is used for reading cities from table of cities in selected region """

    # Selection of cities with html tags from selected region or town, due to different tags 3 types were necessary
    # Td class text is for Brno | headers t1sa... for regions with cities | headers s3 for abroad cities
    sep_cities = polivka.find_all('td', {'class': 'text'})

    if not sep_cities:
        sep_cities = polivka.find_all('td', {'headers': "t1sa1 t1sb2"}) + polivka.find_all('td', {
            'headers': "t2sa1 t2sb2"}) + polivka.find_all('td', {'headers': "t3sa1 t3sb2"})

        if not sep_cities:
            sep_cities = polivka.find_all('td', {'headers': "s3"})

    return sep_cities


def find_url_cities(polivka):
    """ This function is used for searching urls of each city from table in selected region """

    return polivka.find_all('td', {'class': 'cislo'}, 'a href')


def read_table_pandas(cities_url):
    """ This function is used for reading tables under each city from selected region
       (input is url of each city from sel. region) """

    return pd.read_html(cities_url, encoding="UTF-8")


def remove_minus(ins_list):
    """ This function removes minus sign from inserted list """

    if '-' in ins_list:
        ins_list.remove('-')

    return ins_list


def read_table_from_city(cities_url, cislo_obce, obec):
    """ This function is used to join all informations about each city from region,
        output is list with complete informations - 3 inputs - city url, num of city, name of city """

    # local list for collecting all informations
    local_list = []
    global header

    # appending first two informations - num. of city and name of city
    local_list.append(cislo_obce)
    local_list.append(obec)

    # Checking last position (index) in final list
    ind = len(city_list_complete)

    # Calling pandas on each city from region to get all tables
    response = read_table_pandas(cities_url)

    # If this is first run (city_list_complete is empty) we firstly add header and call remove minus func.
    if ind == 0:
        header = (header + list(response[1]["Strana"]["název"]) + list(response[2]["Strana"]["název"]))
        remove_minus(header)

        # Inserting complete header list for each city into final list and increment index
        city_list_complete.insert(ind, header)
        ind += 1

    # Due to different length of the first table under regions (specially abroad),
    # we are checking how many items table contains, if less then 9, using different indexing list for pandas table
    if len(response[0].keys()) < 9:

        # Adding informations from pandas table to local list
        local_list += list(response[0]["Voličiv seznamu"]) + list(response[0]["Vydanéobálky"]) + \
                      list(response[0]["Platnéhlasy"])

        local_list += list(response[1]["Platné hlasy"]["celkem"]) + list(response[2]["Platné hlasy"]["celkem"])

        remove_minus(local_list)

    else:
        # Adding informations from pandas table to local list
        local_list += list(response[0]["Voličiv seznamu"]["Voličiv seznamu"]) + \
                      list(response[0]["Vydanéobálky"]["Vydanéobálky"]) + \
                      list(response[0]["Platnéhlasy"]["Platnéhlasy"])

        local_list += list(response[1]["Platné hlasy"]["celkem"]) + list(response[2]["Platné hlasy"]["celkem"])

        remove_minus(local_list)

    # Inserting complete local list for each city into final list
    city_list_complete.insert(ind, local_list)


def write_data_to_file(file_name):
    """ This function is used for writting all data from final list to csv defined by user """

    f = open(file_name, 'w', encoding="UTF-8")
    f_writer = csv.writer(f)

    print("Writting downloaded data to file:", file_name)
    print(separator)

    # After we are writting all data from final list
    f_writer.writerows(city_list_complete)

    f.close()

# ---------------------------------------------------------------------------------------------------------------------
# Main program:
# ---------------------------------------------------------------------------------------------------------------------


try:
    print(separator)
    print("Welcome - Scrapping from Parlamentni volby 2017".center(len(separator)))
    print(separator)

    # Checking if all arguments are inserted, if not program exits
    if len(sys.argv) != 3:
        raise ValueError
    else:

        # Check if the file has csv suffix or if the name of the file is empty
        if sys.argv[2].split(".")[1] != "csv" or sys.argv[2].split(".")[0] == "":
            raise NameError

        # if it is ok, program try to download inserted url
        response = requests.get(sys.argv[1])

        # check if url is existing (response is 200), if not program exits
        if response.status_code != 200:
            print("You put wrong url, please check it again")
            print(separator)
            exit(1)
        else:
            # If everything is ok, program inform user about url and name of the file
            print("Downloading data from url: ", sys.argv[1], '\n', "Downloaded data will be written to file: ",
                  sys.argv[2], sep="")
            print(separator)

    # Store all object as bs elements in variable soup from requested url stored in response
    soup = bs4.BeautifulSoup(response.text, 'html.parser')

    # Storing cities from selected url
    cities = find_cities(soup)

    # Storing urls from selected url
    cities_url = find_url_cities(soup)

    print("Downloading... Please wait.")
    print(separator)

    # Cycle for sending url of each city from the selected region, send city number and city name,
    # function read tables from each city and collecting all informations in final list
    for index, city in enumerate(cities_url):

        read_table_from_city("https://volby.cz/pls/ps2017nss/"+city.a['href'], city.string, cities[index].string)

    # Calling writting function with second argument (name of the file)
    write_data_to_file(sys.argv[2])

    # Inform user about successfull operation
    print("Process successfully finished. Done.", "\n", "Thank you for choosing my program.", sep="")
    print(separator)

# Inform user in case of some error appears
except requests.exceptions.MissingSchema:
    print("You put wrong url, please check it. Exitting...")
    print(separator)

except FileNotFoundError:
    print("You put wrong name of the file, please check it (e.g. city.csv). Exitting...")
    print(separator)

except ValueError:
    print("You put wrong number of arguments, please check it (arg 1 -> url, arg 2 -> name of csv file). Exitting...")
    print(separator)

except NameError:
    print("You put wrong name of the csv file, please check it (e.g. city.csv). Exitting...")
    print(separator)

except IndexError:
    print("You put wrong name of the csv file, please check it (e.g. city.csv). Exitting...")
    print(separator)

except:
    print("General error appears, please check you run program with arg 1 -> url, arg 2 -> name of csv file,", '\n',
          "you use url: Volby do Poslanecké sněmovny Parlamentu České republiky from 2017.", sep="")
    print(separator)
