from getpass import getpass		#import getpass for secret input for password

TEXTS = ['''
Situated about 10 miles west of Kemmerer,
Fossil Butte is a ruggedly impressive
topographic feature that rises sharply
some 1000 feet above Twin Creek Valley
to an elevation of more than 7500 feet
above sea level. The butte is located just
north of US 30N and the Union Pacific Railroad,
which traverse the valley. ''',
'''At the base of Fossil Butte are the bright
red, purple, yellow and gray beds of the Wasatch
Formation. Eroded portions of these horizontal
beds slope gradually upward from the valley floor
and steepen abruptly. Overlying them and extending
to the top of the butte are the much steeper
buff-to-white beds of the Green River Formation,
which are about 300 feet thick.''',
'''The monument contains 8198 acres and protects
a portion of the largest deposit of freshwater fish
fossils in the world. The richest fossil fish deposits
are found in multiple limestone layers, which lie some
100 feet below the top of the butte. The fossils
represent several varieties of perch, as well as
other freshwater genera and herring similar to those
in modern oceans. Other fish such as paddlefish,
garpike and stingray are also present.'''
]

#Prepared vaules
separator = 79*'-'
count_word = 0
count_title = 0
count_upper = 0
count_lower = 0
count_numbers = 0
count_sum_numbers = 0
clear_list = []
max_word_length = 0
field = dict()				#dict is used for counting length words

allowed_users = {			#allowed users in dict
			
	"bob":"123",
	"ann":"pass123",
	"mike":"password123",
	"liz":"pass123"
				
}

#Control access - registered user - if not success program ends

print("You need to be registered user to use this program, please log in: ")
user_name = input("User name: ").lower()									
user_pass = getpass("User password: ").lower()

if (user_name in allowed_users.keys()):
	if (user_pass == allowed_users[user_name]):
		print(separator)
		print(f"Successfully logged in! Hello {user_name.title()}.")
	else:
		print("Bad credentials! Exitting...")
		exit()
else:
	print("Bad credentials! Exitting...")
	exit()
	
print(separator)




#If log in is successful, offer user which text should bec analyzed

print("There are three text, which could be analyzed.")
choosed_text = input("Please choose, which text will be analyzed [1-3]: ")
print(separator)



#Check if user put valid options:

if not choosed_text.isnumeric() or int(choosed_text) >= 4 or int(choosed_text) <= 0:
	print("You put wrong number or you put non-numeric value")
	exit()
else:
	#lower index due to indexation (begin 0)
	choosed_text = int(choosed_text) - 1			
	

#Make list of words without spaces,commas and dots
clear_list = TEXTS[choosed_text].replace(',',"").replace('.',"").split()


#_First FOR cycle for checking - no. words, title words...
for word in clear_list:
	
		
	if word.istitle():
		
		count_title += 1
		
	elif word.isupper() and word.isalpha():
		
		count_upper += 1
	
	elif word.islower() and word.isalpha():
	
		count_lower += 1
	
	elif word.isnumeric():
	
		count_numbers += 1
		count_sum_numbers += int(word)
	
	if max_word_length < int(len(word)):
		max_word_length = int(len(word))
	
		
	count_word += 1

print(f"There are {count_word} words in the selected text.")
print(f"There are {count_title} titlecase words.")
print(f"There are {count_upper} uppercase words.")
print(f"There are {count_lower} lowercase words.")
print(f"The longest word in text has {max_word_length} letters.")
print(f"There are {count_numbers} numeric strings.")
print(f"The sum of all the numbers: {count_sum_numbers}")
print(separator)

#Second part - how often length of the words appears

#Counting how many times appears words base on their length
for word in clear_list:

	field[int(len(word))] = field.get(int(len(word)),0) + 1

	
max_delka = max(field.values()) #Find max of how many times apperas the word
delka = (int((max_delka - 10)/2)) * " " #How many spaces between len and middle word and NR and middle word
delka2 = (max_delka - (int((max_delka - 10)/2) + 10)) * " " #Rest spaces max length - (first len + len of word occurences)
title = ("LEN|",delka,"OCCURENCES",delka2,"|NR.") #put everything together
print("".join(title)) # print with join func.

#Printing content of dict in formatted form
for key, value in field.items():
	print(f"{key:>3}|{value * '*':<{max(field.values())}}|{value:<3}")

print(separator)
print("""Thank you for using our text analyzator, made as project 1
in Engeto academy by Tomas Zak. Have a nice day!""")
print(separator)

















