"""
This is project 2 for Engeto academy - game - Bulls and cows
Made by Tomas Zak
"""

# Imported libraries
import random                                   # library random for picking random num. from list
from datetime import datetime                   # datetime library for time purposes -> how long game takes

# Global variables
bulls_cows = {"bulls": 0, "cows": 0}            # final dict, where are stored guessed numbers
separator = "-" * 80                            # seperator used for print


def generate_number_list() -> list:
    """This function generate list of numbers"""
    return list(range(0, 10))                   # return number line 0 -> 9


def generate_guessed_number(num_list: list) -> list:
    """This function generate 4-digit number stored in list with unique numbers from list of numbers"""

    # local variables - list guess -> return 4-digit unique list
    guess = []
    index = 0

    while index < 4:
        rand_num = random.choice(num_list)      # randomly choose number from list

    # Condition for checking if first number is not 0, if yes skip it and continue with generating new one
        if index == 0 and rand_num == 0:
            continue
        else:
            guess.insert(index, str(rand_num))  # take one number from var. num_list and place it in new list guess
            num_list.remove(rand_num)           # remove this number from orig. list to avoid repeating
            index += 1
    return guess


def user_number() -> str:
    """This function is used for user input"""
    return input("Enter number (4-digit): ")    # ask user for input


def validate_user_input(usr_number: str) -> bool:
    """This function validate user input if all conditions are met"""

    # This function controls if number entered by user is not starting with 0, are only numbers and have right length

    if not usr_number.isnumeric():
        print("Input is not containing only numbers")
        return False
    elif usr_number[0] == "0":
        print("Number can't start with 0")
        return False
    elif len(usr_number) < 4 or len(usr_number) > 4:
        print("Entered number is no 4-digit long")
        return False
    elif len(set(usr_number)) != len(usr_number):
        print("Entered number contains same digit!")
        return False
    else:
        return True


def compare_user_num_and_guess_num(guess_num: list, us_num: list) -> None:
    """This function compare user number with random generated number and return bulls, cows"""

    # Each run is necessary tu reset global dict bulls and cows
    bulls_cows["bulls"] = 0
    bulls_cows["cows"] = 0

    # This cycle controls if entered number hits bulls or cows (right position or if the number is in gen. number).
    for index, number in enumerate(guess_num):
        if number == us_num[index]:
            bulls_cows["bulls"] += 1
        elif number in us_num:
            bulls_cows["cows"] += 1
        else:
            continue


if __name__ == "__main__":

    list_of_numbers = generate_number_list()                    # list of unique numbers 0 -> 9
    guessed_number = generate_guessed_number(list_of_numbers)   # 4-digit list with unique numbers
    number_tries = 0                                            # counter for counting number of tries

    # For testing purposes
    # usr_num = user_number()
    # print(guessed_number)

    start_time = datetime.now()                                 # take actual time when game starts
    start_time = start_time.strftime("%H:%M:%S")                # convert time string to only hours, min, sec

    print(separator)
    print("Welcome to game - BULLS and COWS, let's play!\nI generated 4-digit number and you have to guess it!")
    print(f"Game started at: {start_time}")
    print(separator)

    while True:
        """This cycle is used for control main function"""

        usr_num = user_number()                                         # ask user for input number
        number_tries += 1

        if validate_user_input(usr_num):                                # check if the user's num. is ok if no, continue

            compare_user_num_and_guess_num(guessed_number, list(usr_num))   # Call compare func. for bulls/cows

            if bulls_cows["bulls"] == 4:                                # if all digit are on right position, game ends

                stop_time = datetime.now()                              # take time in the end of the game
                stop_time = stop_time.strftime("%H:%M:%S")              # convert it to hour, sec, min
                FMT = '%H:%M:%S'                                        # time format for time difference
                time_delta = datetime.strptime(stop_time, FMT) - datetime.strptime(start_time, FMT)  # time difference
                time_delta = str(time_delta).split(":")                 # split time difference by semicolon for print
                print(f"Game finished at: {stop_time}")
                print("Congratulation!\nYou won! Guessed number was: ", "".join(guessed_number), "\n",
                      "Number of tries: ", number_tries, sep="")
                print(f"Game finished after: {time_delta[0]} h {time_delta[1]} min {time_delta[2]} sec")
                print(separator)
                break

            else:

                if bulls_cows["bulls"] <= 1:                            # for right spelling bulls, if < 2 only bull
                    str_bull = "Bull"
                else:
                    str_bull = "Bulls"

                if bulls_cows["cows"] <= 1:                             # for right spelling cows, if < 2 only cow
                    str_cow = "Cow"
                else:
                    str_cow = "Cows"

                print(f"{str_bull}: ", bulls_cows["bulls"], f", {str_cow}: ", bulls_cows["cows"], sep="")
                print(separator)
        else:
            continue                                                    # if user input is wrong, skip this cycle
